import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint

outputfile = 'omegavalues.txt'
writefile = open(outputfile, 'w')
xvalues = []
omegavalues = []

divisions = 100
portion = 1.0/10 #Determines how big to make the middle, which we're actually graphing
plots = 3

def fillzeros(n, a):
	temp = str(n)
	while (a - len(temp)) > 0:
		temp = '0' + temp
	return temp

def printline(a, b):
	line = ''
	for values in range(0, len(a)):
		if b[values] == 0:
			line = line + str(0) + '\t'
			continue
		line = line + str(1.0 - float(a[values]/b[values])) + '\t'
	line = line + '\b\b\n'
	writefile.write(line)

def extract(a):
	last = 0
	values = []
	for char in range(0, len(a)):
		if a[char].isdigit() or a[char] == '-' or a[char]=='.' or a[char] =='e' or a[char] == '+':
			continue
		if last < char:
			values.append(float(a[last:char]))
		last = char + 1
	if last < len(a):
		values.append(float(a[char:]))
	return values

read = open('omegas.txt', 'r') #fix this, it sucks

for line in read:
	number = ''
	temp = []
	for value in line:
		if value == ' ' or value == '\n':
			try:
				float(number)
			except ValueError:
				break
			temp.append(float(number))
			number = ''
			continue
		number = number + value
	omegavalues.append(temp)

for value in range(0, len(omegavalues[0])):
	xvalues.append(value)

fig = plt.figure(1, figsize=(8,8))


filestodelete = []
for i in os.listdir("Images/"):
	filestodelete.append(i)
	
for i in filestodelete:
	os.system("rm Images/" + i)
	
# Plot graph of Omega vs. x at various points in time

for a in range(0, len(omegavalues)):
	ax1 = fig.add_subplot(111)
	ax1.plot(np.asarray(xvalues), np.asarray(omegavalues[a]))
	ax1.set_xlabel('x')
	ax1.set_xlim(min(xvalues),max(xvalues))
	ax1.set_ylabel('Omega')
	ax1.set_ylim([0,max(omegavalues[len(omegavalues)-1])])
	ax1.set_ylim(0,1)

# ax2 = fig.add_subplot(111)
# ax2.plot(np.asarray(xvalues), np.asarray(allbins[1]))
# ax2.set_xlabel('x')
# ax2.set_ylabel('Omega')
#
# ax3 = fig.add_subplot(111)
# ax3.plot(np.asarray(xvalues), np.asarray(allbins[2]))
# ax3.set_xlabel('x')
# ax3.set_ylabel('Omega')

	#plt.tight_layout()
	plt.savefig('Images/file' + str(fillzeros(a,len(str(len(omegavalues))))) + '.png')
	plt.clf()
os.system("convert -delay 2 -quality 100 Images/*.png Images/file.mpeg")
