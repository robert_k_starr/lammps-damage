//
// Created by Robert Starr on 2019-07-20.
//

#ifndef C___FILES_REWRITE_PARAMETERS_H
#define C___FILES_REWRITE_PARAMETERS_H

#define XATOMS 400
#define YATOMS 100
#define ZATOMS 1

#define BUFFER .01

#define SPACING 1

#define GAUSSHEIGHT .3
#define SIGMA 25

#define WALKERFILES 5

#endif //C___FILES_REWRITE_PARAMETERS_H
