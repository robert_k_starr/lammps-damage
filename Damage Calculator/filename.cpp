#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <fstream>
#include <sstream>
#include <dirent.h>

#include "filename.hpp"
#include "functions.hpp"

using namespace std;

filename::filename(string a, string c){
	base = a;				// base
	folder = 0;			// folder number
	maxfolder = 0;
	type = c;				// id or bond
	currentfile = 0;
	digits = 0;

	struct dirent *dirp;
	DIR *dp;

	while ((dp  = opendir(returnfolder().c_str())) == NULL) {
		returnfolder();
		digits++;
}
	while (1){
		folder++;
		if ((dp  = opendir(returnfolder().c_str())) == NULL) break;
	}
	maxfolder = folder;

	folder = 0;
	dp = opendir(returnfolder().c_str());
	int typesize = type.size();
	while((dirp = readdir(dp))!= NULL){
		string testfile = string(dirp->d_name);
		if (testfile.find(type) != string::npos) {
			int numfiles = files.size();
			for (int i = 0; i < files.size(); i++){
				int currentfilenumber = stoi(files[i].substr(typesize,files[i].size() - typesize - 4));
				int newfilenumber = stoi(testfile.substr(typesize, testfile.size() - typesize - 4));
				if (currentfilenumber > newfilenumber){
					vector<string>::iterator it = files.begin() + i;
					files.insert(it,testfile);
					break;
				}
			}
			if (numfiles == files.size()) files.push_back(testfile);
		}
	}

	maxfile = files.size();

}

string filename::returnfolder() {return base + changetostring(folder, digits);}

string filename::returnfile() {return returnfolder() + "/" + files[currentfile];} // Fix this

bool filename::nextfolder() {
	folder++;
	if (folder >= maxfolder) folder = 0;
	return folder;
}

bool filename::nextfile() {
	currentfile++;
	if (currentfile >= maxfile) currentfile = 0;
	return currentfile;
}

int filename::folders(){ return maxfolder;}
