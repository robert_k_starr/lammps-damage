//
// Created by Robert Starr on 2019-07-20.
//
#include <math.h>

#include "atom.h"
#include "parameters.h"
#include "functions.h"

atom::atom(int x, int y, int z){
    init(x, y, z);
}

atom::atom(double *value){
    init(value[0], value[1], value[2]);
}

atom::atom(){
    for (int i = 0; i < 3; i++) relativeCoords[i] = -1;
    realAtom = false;
}

void atom::setID(int id){
    int temp =  atomID;
    atomID = id;
}

void atom::init(double x, double y, double z){
    relativeCoords[0] = x;
    relativeCoords[1] = y;
    relativeCoords[2] = z;

    absoluteCoords[0] = x;
    absoluteCoords[1] = y;
    absoluteCoords[2] = z;
    convertToAbsolute(absoluteCoords);
    realAtom = true;
    atomID = -1;
}

void atom::convertToAbsolute(double *coords) {
    coords[0] = (coords[0] + (((int) coords[1])%2)/2.0)*SPACING;
    coords[1] = (coords[1]*sqrt(3)/2.0)*SPACING;
    coords[2] = (coords[2])*SPACING;
}

void atom::convertToRelative(double *coords) {
    coords[2] = (coords[2])/SPACING;
    coords[1] = (coords[1])/(SPACING*sqrt(3)/2.0);
    coords[0] = coords[0]/(SPACING) - (((int) coords[1])%2)/2.0;
}

double atom::distanceToAtom(atom *otherAtom) {
    return distance(returnAbsoluteCoords(), otherAtom->returnAbsoluteCoords());
}

std::vector<double> atom::returnAbsoluteCoords(){
    std::vector<double> atomCoords;

    for (int i = 0; i < 3; i++) atomCoords.push_back(absoluteCoords[i]);

    return atomCoords;
}

int atom::returnID() {
    return atomID;
}

string atom::outputAtom() {
    string temp = to_string(atomID) + " 0 1 ";

    for (int i = 0; i < 3; i++){
        temp = temp + to_string(absoluteCoords[i]) + " ";
    }

    return temp;
}
