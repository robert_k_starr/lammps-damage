newton off

variable SQRT3 equal sqrt(3)

lattice custom 1.0 a1 1.0 0.0 0.0 a2 0.0 ${SQRT3} 0.0 a3 0.0 0.0 1.0 basis 0.0 0.0 0.0 basis .5 .5 0

variable MASS equal 28.06

variable TRIAL_NUMBER equal 4

variable WIDTH equal 400
variable XATOMS equal 400
variable YATOMS equal 100
variable XMIDDLELOW equal ${XATOMS}/2
variable XMIDDLEHIGH equal ${WIDTH}/2+.51

variable HEIGHT equal 86.6025
variable YMIDDLELOW equal ${YATOMS}/4
variable YMIDDLEHIGH equal ${YATOMS}/4+.51

variable DEPTH equal 1.0
variable ENERGY equal 5.96
variable XX0 equal 1.3 # ratio of x_f to x_0
variable CUTOFF equal 10*${XX0}
variable SPRINGCONST equal 2*${ENERGY}/(${XX0}-1)^2
variable RANDOM equal random(0,4,1)
variable MIDFORCE equal ${RANDOM}*${SPRINGCONST}

variable EPSILON equal .01
variable SIGMA equal (2)^(-1/6)
variable GAMMA equal sqrt(${MASS}*${SPRINGCONST})*(${XX0}-1)/2
variable SPEED equal .0001
variable NEGSPEED equal -${SPEED}
variable TIMESTEP equal 1
variable LENGTH equal 2*abs(${WIDTH}*(${XX0}-1)/(${SPEED}*${TIMESTEP}))/4
variable OUTPUTEVERY equal 10000/(${TIMESTEP})
variable FORCE equal ${SPRINGCONST}/${HEIGHT}
variable NEGFORCE equal -${FORCE}
variable TEMPERATURE equal 400

variable frame equal elaplong
variable time equal "v_SPEED*elaplong"
variable righttime equal "v_WIDTH - v_time"

region Sample block 0.0 ${WIDTH} 0.0 ${HEIGHT} 0.0 ${DEPTH}

variable RIGHTEDGE equal ${WIDTH}-2.0
variable TOPEDGE equal ${HEIGHT}-1.0

variable RIGHTMIDDLE equal 2*${WIDTH}/3-.5
variable RIGHTMIDDLEEND equal ${RIGHTMIDDLE}+.5
variable LEFTMIDDLE equal ${WIDTH}/3-.5
variable LEFTMIDDLEEND equal ${LEFTMIDDLE}+.5

variable HALFHEIGHT equal ${HEIGHT}/2
variable HALFHEIGHTTOP equal ${HALFHEIGHT}+.45

variable HALFWIDTH equal ${WIDTH}/2

variable HALFDEPTH equal ${DEPTH}/2
variable HALFDEPTHTOP equal ${HALFDEPTH}+1

region Left block 0.0 2.0 0.0 ${HEIGHT} 0.0 ${DEPTH}
region Right block ${RIGHTEDGE} ${WIDTH} 0.0 ${HEIGHT} 0.0 ${DEPTH}
region Bottom block 0.0 ${WIDTH} 0.0 1.0 0.0 ${DEPTH}
region Top block 0.0 ${WIDTH} ${TOPEDGE} ${HEIGHT} 0.0 ${DEPTH}
region Lefthalf block 1.5 ${HALFWIDTH} 0.0 ${HEIGHT} 0.0 ${DEPTH}
region Righthalf block ${HALFWIDTH} ${WIDTH} 0.0 ${HEIGHT} 0.0 ${DEPTH}
region Interior union 2 Lefthalf Righthalf
region LeftHole block ${LEFTMIDDLE} ${LEFTMIDDLEEND} ${HALFHEIGHT} ${HALFHEIGHTTOP} 0.0 1.0
region RightHole block ${RIGHTMIDDLE} ${RIGHTMIDDLEEND} ${HALFHEIGHT} ${HALFHEIGHTTOP} 0.0 1.0
region TopHalf block 0.0 ${WIDTH} 0.0 ${HALFHEIGHT} 0.0 ${DEPTH}
region Slice block 0.0 ${WIDTH} 0.0 ${HEIGHT} 0.0 1.0
region Lefthalfslice block 1.5 ${HALFWIDTH} 0.0 ${HEIGHT} 0.0 1.0

thermo_modify lost warn lost/bond warn