//
// Created by Robert Starr on 2019-07-20.
//

#include <math.h>

#include "output.h"
#include "parameters.h"
#include "functions.h"

void printFile(string filename, string textToPrint){
    ofstream file;

    file.open(filename, ios::app);
    file << textToPrint;
    file.close();
}

void cleanFile(string filename){
    ofstream file;
    file.open(filename);
    file.close();
}

void initialBondsFile(string filename, vector<atom *> atomList, vector<bond *> bondList) {
    string fileToRecord;

    cleanFile(filename);
    int atomListLength = atomList.size();
    int bondListLength = bondList.size();
    fileToRecord = to_string(atomListLength) + " " + to_string(bondListLength) + " ";
    fileToRecord = fileToRecord + "0 " + to_string(XATOMS) + "\n";

    fileToRecord = fileToRecord + outputBonds(bondList) + "\n";


    printFile(filename, fileToRecord);
}

void produceLAMMPSFile(string filename, vector<atom *> atomList, vector<bond *> bondList) {
    string tempOutput = "LAMMPS Data File \n\n";
    tempOutput = tempOutput + to_string(atomList.size()) + " atoms\n";
    tempOutput = tempOutput + to_string(intactBonds(bondList)) + " bonds\n\n";

    tempOutput = tempOutput + "3 atom types\n2 bond types\n\n";

    tempOutput = tempOutput + "0 " + to_string(XATOMS*SPACING) + " xlo xhi\n";
    tempOutput = tempOutput + "0 " + to_string(YATOMS*SPACING*sqrt(3)/2) + " ylo yhi\n";
    tempOutput = tempOutput + "0 " + to_string(ZATOMS*SPACING) + " zlo zhi";

    tempOutput = tempOutput + "\n\nAtoms\n\n";

    tempOutput = tempOutput + outputAtoms(atomList);

    tempOutput = tempOutput + "\nBonds\n\n";

    tempOutput = tempOutput + outputBonds(bondList);
    tempOutput = tempOutput + "\n";

    cleanFile(filename);
    printFile(filename, tempOutput);
}

string outputAtoms(vector<atom *> atomList) {
    string finalString = "";
    int length = atomList.size();

    for (int i = 0; i < length; i++) {
        finalString = finalString + atomList[i]->outputAtom() + "\n";
    }

    return finalString;
}

string outputBonds(vector<bond *> bondList) {
    string finalString = "";
    int length = bondList.size();

//    reIDBonds(bondList);

    for (int i = 0; i < length; i++) {
        if (bondList[i]->isActive()) {
            finalString = finalString + bondList[i]->outputBond() + "\n";
        }
    }

    return finalString;
}
