//
// Created by Robert Starr on 2019-07-20.
//

#include "bond.h"
#include "functions.h"

bond::bond(atom * atom1, atom * atom2){
    this->atom1 = atom1;
    this->atom2 = atom2;
    this->id = -1;
    this->type = 1;
    this->active = true;
}

void bond::setID(int id){
    this->id = id;
}

int bond::returnID() {
    return this->id;
}

void bond::setType(int type) {
    this->type = type;
}

vector<double> bond::bondMidpoint() {
    return midpoint(*atom1, *atom2);
}

string bond::outputBond() {
    string outputCoords = to_string(id) + " " + to_string(type) + " " +
            to_string(atom1->returnID()) + " " + to_string(atom2->returnID());

    return outputCoords;
}

void bond::deleteBond() {
    this->active = false;
}

void bond::unDeleteBond() {
    this->active = true;
}

bool bond::isActive() {
    return this->active;
}
