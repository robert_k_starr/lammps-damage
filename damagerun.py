import os
import datetime
import math
import sys
from shutil import *

deletebox = []

filename = 'in.3ddamage'
initFile = 'initFile.txt'
datafile = './C++ Files/parameters.h'

atomtypes = 0
bondtypes = 0

data = open(datafile, "r")
init = open(initFile, "r")

COLUMNS = 0
ROWS = 0

for line in data:
	if 'XATOMS' in line:
		for place in range(0, len(line)):
			if line[place].isdigit():
				COLUMNS = int(line[place: len(line)])
				break
	if 'YATOMS' in line:
		for place in range(0, len(line)):
			if line[place].isdigit():
				ROWS = int(line[place: len(line)])
				break
data.close()

width = COLUMNS ## FIX THIS
height = '%.4f'%(ROWS*math.sqrt(3)/2)
depth = 0
scale = 0

initFileString = ""

for line in init:
	if "variable WIDTH equal" in line:
		line = "variable WIDTH equal " + str(width) + "\n"
	if "variable HEIGHT equal" in line:
		line = "variable HEIGHT equal " + str(height) + "\n"
	if "variable XATOMS equal" in line:
		line = "variable XATOMS equal " + str(COLUMNS) + "\n"
	if "variable YATOMS equal" in line:
		line = "variable YATOMS equal " + str(ROWS) + "\n"
	if "variable DEPTH equal" in line:
		depth = float(line[len("variable DEPTH equal") + 1: len(line) - 1])
	if line[0: len("lattice")] == "lattice":
		start = 0
		end = 0
		for value in range(len("lattice"), len(line) - 1):
			if line[value] != " " and line[value] != "\t":
				start = value
				break
		for value in range(start, len(line) - 1):
			if line[value] == " " or line[value] == "\t":
				start = value
				break
		for value in range(start, len(line) - 1):
			if line[value] != " " and line[value] != "\t":
				start = value
				break
		for value in range(start, len(line) - 1):
			end = len(line) - 1
			if line[value] == " " or line[value] == "\t":
				end = value - 1
				break
		scale = float(line[start: end])
	initFileString = initFileString + line

init.close()
init = open(initFile, "w")
init.write(initFileString)
init.close()

for files in os.listdir('./Frames'):
	if files[0] != ('.'):
		deletebox.append(files)
for files in deletebox:
	if os.path.isdir('./Frames/' + files):
		rmtree('./Frames/' + files)
	else:
		os.remove('./Frames/' + files)

text = open(filename, "r")
textcopy = open(filename + "temp", "w")

os.system("python displacement.py")

for file in os.listdir('WalkerFiles'):
 if 'walkerfile' not in file:
 	continue
 text = open(filename, "r")
 textcopy = open(filename + "temp", "w")
 walkerfile = 'WalkerFiles/' + file
 os.system('mkdir Frames/' + file[:-4])
 
 for line in text: 
	if 'Frames/' in line:
		start = line.index('/') + 1
		line = line[:start] + file[:-4] + '/' + line[start:]
	if ("bond_coeff" in line) and (line[0] != '#'):
		start = len("bond_coeff") + 1
		end = line.find(" ", start) + 1
		tempbond = float(line[start: end])
		if tempbond > bondtypes:
			bondtypes = tempbond
	if line[0: len("mass")] == "mass":
		start = 0
		end = 0
		for value in range(4, len(line) - 1):
			if line[value] != " " and line[value] != "\t":
				start = value
				break
		for value in range(start, len(line) - 1):
			if line[value] == " " or line[value] == "\t":
				end = value - 1
		atomstemp = float(line[start: end])
		if atomstemp > atomtypes:
			atomtypes = atomstemp
	if 'read_data' in line:
		line = 'read_data ' + walkerfile
	textcopy.write(line)

 width = scale*width
 height = scale*float(height)
 depth = scale*depth

 for line in text:
	 if "variable DEPTH equal" in line:
		 depth = float(line[len("variable DEPTH equal") + 1: len(line) - 1])
		 break

 text.close()
 textcopy.close()

 if 'lmp_mpi' in os.listdir('./'):
	 print 'lmp_mpi'
	 os.system("mpirun -np 2 lmp_mpi < " + filename + "temp")
 else:
	 os.system("ibrun ../lmp_stampede <" + filename + "temp")
 outputfolder = 'Frames/' + file[:-4]
 os.system("convert -delay 2 -quality 100 " + outputfolder + "/*.jpg " + outputfolder + "/a.mpeg")

os.system("python calculateomega.py")