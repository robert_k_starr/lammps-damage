import os

values = open("forces.txt", 'w')

for files in os.listdir('./Frames'):
	start = False
	sum = 0
	if (files[-4: -1] + files[-1] == '.txt'):
		data = open('./Frames/' + files, 'r')
		for line in data:
			if start:
				sum = sum + float(line)
			if not start:
				if (line == "ITEM: ENTRIES c_1 \n"):
					start = True
	values.write(str(sum) + "\n")
	
values.close()