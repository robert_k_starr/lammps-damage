#include <iostream>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <fstream>
#include <sstream>
#include <unistd.h>
#include <string>

#include "functions.hpp"

using namespace std;

//template <size_t size_x, size_t size_y>
std::vector<vector<int>> ArraytoVec(int array[][2], int length){
	vector<vector<int>> temp;

	for (int i = 0; i < length; i++) {
		temp.push_back({array[i][0], array[i][1]});
		}

	return temp;
}

string changetostring(int value, int spaces){
	int temp = value;
	int digits = -1;
	if (value == 0) digits = 0;
	while (temp){
		temp = temp/10;
		digits = digits + 1;
	}

	string returnvalue;

	for (int i = 0; i < spaces - digits -1; i++) returnvalue = returnvalue + "0";
	returnvalue = returnvalue + to_string(value);
	return returnvalue;
}

void trackatoms(float *atoms, std::string line){
	string tempvalue;
	int atomnumber = -1;
	float xvalue = -1;
	bool print = true;
	int spaces = 0;
	for (int i = 0; i < line.length(); i++){
		if (print){
			if (line[i] == ' '){
				if (atomnumber >= 0) xvalue = std::stof(tempvalue);
				else {
					atomnumber = std::stoi(tempvalue);
				}
				tempvalue.clear();
				print = false;
				spaces = spaces + 1;
				continue;
			}
			tempvalue = tempvalue + line[i];
		}
		if (line[i]== ' ') spaces = spaces + 1;
		if (spaces == 2) print = true;
	}
	if (atomnumber >=0) atoms[atomnumber] = xvalue;
}

void recordbonds(int *bonds, std::string line){
	string tempvalue;
	for (int i = 0; i < line.length() - 1; i++){
		if (line[i] == ' ') {
			bonds[0] = std::stoi(tempvalue);
			tempvalue.clear();
			continue;
		}
		tempvalue = tempvalue + line[i];
	}
	bonds[1] = std::stoi(tempvalue);
}

vector<int> recordbonds(std::string line){
	int bonds[2];
	recordbonds(bonds, line);
	vector<int> temp;
	temp.push_back(bonds[0]);
	temp.push_back(bonds[1]);
	
	return temp;
}

void binbonds(int *bins, int bincount, float binmin, vector<vector<int>> abonds, int numberofbonds, float *atoms){
	for (int i = 0; i < bincount; i++) bins[i] = 0;
	for (int i = 0; i < numberofbonds; i++){
		float positiona = min(atoms[abonds[i][0]], atoms[abonds[i][1]]);
		float positionb = max(atoms[abonds[i][0]], atoms[abonds[i][1]]);
		for (int j = 0; j < bincount; j++){
			if ((positiona <= binmin + j*binsize) and (positionb >= binmin + j*binsize)) bins[j]++;
		}
	}
}

vector<float> analyzefile(string walkerstring, string bondstring, vector<vector<int>> bonds){

	ifstream walkerfile(walkerstring);
	ifstream bondfile(bondstring);
	cout << bondstring << "\n";
	string atomline, bondline;
	vector<float> results(bondcount,0);
	
	float atoms[atomcount + 1];
	
	int currentbondcount = 0;
	while (getline(walkerfile, atomline)){
		
		if (atomline.find("ITEM: ATOMS") != string::npos){
			// Establish current positions of all the atoms in the simulation

			while (getline(walkerfile, atomline)){

				trackatoms(atoms, atomline);
			}
		}
	}

	while (getline(bondfile, bondline)){

		if (bondline.find("ITEM: NUMBER OF ENTRIES") != string::npos) {
			getline(bondfile, bondline);
			currentbondcount = stoi(bondline);
		}

		if (bondline.find("ITEM: ENTRIES c_3") != string::npos){	
			int allbondsbin[bincount];
			int currentbondsbin[bincount];
			vector<vector<int>> currentbondsvector;
			
			while(getline(bondfile, bondline)){
				currentbondsvector.push_back(recordbonds(bondline));
			} 

 			binbonds(currentbondsbin, bincount, binmin, currentbondsvector, currentbondcount, atoms);
			binbonds(allbondsbin, bincount, binmin, bonds, bondcount, atoms);
			
			for (int i = 0; i< bincount; i++) results[i] = ((float)allbondsbin[i] - (float)currentbondsbin[i])/((float)allbondsbin[i]);
 			currentbondsvector.clear();
		}
		
   
   
	}
	walkerfile.close();
	bondfile.close();
	
	return results;
}
