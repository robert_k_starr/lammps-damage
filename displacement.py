import sys
import os
import numpy as np
from math import *
from shutil import *

inputfile = ''

results = []
runs = 5

## Maybe something to ensure that the weight function is symmetric?
## Switch to numpy arrays

sourceFolder = "./DisplacementFiles/"

def convert(stringvalue):
	returnvalue = []
	temp = ""
	for a in stringvalue:
		if (a == " ") or (a == "\n"):
			returnvalue.append(float(temp))
			temp = ""
		else:
			temp += a
	return returnvalue
	
class atom:
	def __init__(self, line):
		inputline = []
		temp = ""
		for a in line:
			if (a == " ") or (a == "\n"):
				if len(temp) == 0:
					continue
				inputline.append(float(temp))
				temp = ""
			else:
				temp += a
		if len(inputline) == 8:
			self.id = int(inputline[0])
			self.type = int(inputline[1])
			self.coords = (inputline[2], inputline[3], inputline[4])
			self.forces = (inputline[5], inputline[6], inputline[7])
			self.maxdisp = 0.0
			self.maxforce = 0.0
		else:
			self.id = False
	
	def printvalues(self):
		print "Atom #" + str(self.id) + " is at " + str(self.coords)
	
	def displacement(self, x):
		temp = 0
		if len(x) != 3:
			return False
		for a in range(0, 3):
			temp = temp + (self.coords[a] - x[a])**2
		a = sqrt(temp)
		if a > self.maxdisp:
			self.maxdisp = a
	
	def force(self, f):
		temp = 0
		if len(f) != 3:
			return False
		for a in range(0, 3):
			temp = temp + (f[a])**2
		a = sqrt(temp)
		if a > self.maxforce:
			self.maxforce = a
	
	def returnid(self):
		return self.id
	
	def returncoords(self):
		return self.coords
	
	def returnforces(self):
		return self.forces

deletebox = []

for files in os.listdir(sourceFolder):
	if files[0] != ('.'):
		deletebox.append(files)
for files in deletebox:
	if os.path.isdir(sourceFolder + files):
		rmtree(sourceFolder + files)
	else:
		os.remove(sourceFolder + files)

filelist = []
filenumbers = []

for runtime in range(0,runs):
	print "TRIAL NUMBER " + str(runtime)
	numberbins = 10
	bins		= [0]*numberbins
	totalatoms	= [0]*numberbins
	atoms 		= []
	origin 		= 0
	barlength	= 100	## Corresponds to L in Barrenblatt
	
	initFile = open('initFile.txt', 'r')
	initString = ""
	for line in initFile:
		if "variable TRIAL_NUMBER equal" in line:
			line = "variable TRIAL_NUMBER equal " + str(runtime) + "\n"
		initString = initString + line
	
	initFile.close()
	initFile = open('initFile.txt', 'w')
	initFile.write(initString)
	initFile.close()
	
	currentFolder = sourceFolder + "TRIAL" + str(runtime)
	os.system("mkdir " + currentFolder)
	
	os.system("./lmp_mpi < in.displacement")

	for files in os.listdir(currentFolder):
		if '.txt' not in files:
			continue
		for a in range(0,len(files)):
			if files[a].isdigit():
				for b in range(a + 1, len(files)):
					if files[b] == '.':
						filenumbers.append(files[a:b])
				break

	for a in range(0, len(filenumbers) - 1):
		for b in range(a + 1, len(filenumbers)):
			if int(filenumbers[a]) > int(filenumbers[b]):
				temp = filenumbers[a]
				filenumbers[a] = filenumbers[b]
				filenumbers[b] = temp

	for a in filenumbers:
		filelist.append(currentFolder + '/forces' + a + '.txt')

	inputfile = filelist[0]

	inputdata = open(inputfile, 'r')
	inputlines = []
	x = [0,0]
	y = [0,0]

	for line in inputdata:
		if "ITEM" in line:
			continue
		inputlines.append(line)

	x = convert(inputlines[2])
	y = convert(inputlines[3])

	binsize = (x[1] - x[0])/float(numberbins)
	maxforce = 0
	for linenumber in range(5, len(inputlines)):
		tempatom = atom(inputlines[linenumber])
		if len(atoms) == 0:
			atoms.append(tempatom)
		elif atoms[-1].returnid() < tempatom.returnid():
			atoms.append(tempatom)
		else:
			for a in range(0, len(atoms)):
				if atoms[a].returnid() > tempatom.returnid():
					temp1 = atoms[:a]
					temp1.append(tempatom)
					temp2 = atoms[a:]
					atoms = temp1 + temp2
					break
		if atoms[-1].forces[0] > maxforce:
			origin = atoms[-1].returncoords()[0]
			maxforce = atoms[-1].forces[0]
	n = 0

	for files in filelist:
		n = n + 1
		inputfile = files
		tempatoms = []
		inputdata = open(inputfile, 'r')
		inputlines = []
		for line in inputdata:
			if "ITEM" in line:
				continue
			inputlines.append(line)
		for linenumber in range(5, len(inputlines)):
			tempatom = atom(inputlines[linenumber])
			if len(tempatoms) == 0:
				tempatoms.append(tempatom)
			elif tempatoms[-1].returnid() < tempatom.returnid():
				tempatoms.append(tempatom)
			else:
				for a in range(0, len(tempatoms)):
					if tempatoms[a].returnid() > tempatom.returnid():
						temp1 = tempatoms[:a]
						temp1.append(tempatom)
						temp2 = tempatoms[a:]
						tempatoms = temp1 + temp2
						break
		for space in range(0, len(atoms)):
			a = tempatoms[space]
			b = atoms[space]
			if (a.returnid() != b.returnid()) or (not a.returnid()):
				print a.returnid()
				print b.returnid()
				print space
				print "\n"
				break
			b.displacement(a.returncoords())
			b.force(a.returnforces())

	sum 		= 0
	sumforces 	= 0
	lambdasq 	= 0
	lambdaforce	= 0

	for a in atoms:
		if a.type != 1:
			continue
		sum 		+= abs(a.maxdisp)
		sumforces 	+= a.maxforce
		lambdasq 	+= .5*a.maxdisp*(a.returncoords()[0] - origin)**2
		lambdaforce	+= .5*a.maxforce*(a.returncoords()[0] - origin)**2

	results.append(str(lambdaforce/float(sumforces)/barlength**2) )

sum = 0
for a in results:
	print a
	sum = sum + float(a)

# Something's wrong here...
print "Average of lambda^2 is: " + str(sum/len(a))