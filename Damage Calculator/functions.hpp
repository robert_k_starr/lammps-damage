#ifndef functions_hpp
#define functions_hpp

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>

#include "filename.hpp"

using namespace std;

std::vector<vector<int>> ArraytoVec(int array[][2], int);

string changetostring(int, int);

void trackatoms(float*, string);
void recordbonds(int*, string);
vector<int> recordbonds(string);

void binbonds(int*, int, float, vector<vector<int>>, int, float*);
vector<float> analyzefile(string, string, vector<vector<int>>);

extern int atomcount, bondcount, bincount;
extern float binsize, binmin, binmax;

#endif /* functions_hpp */
