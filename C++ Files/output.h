//
// Created by Robert Starr on 2019-07-20.
//

#ifndef C___FILES_REWRITE_OUTPUT_H
#define C___FILES_REWRITE_OUTPUT_H

#include <fstream>
#include <cstring>
#include <sstream>
#include <vector>

#include "atom.h"
#include "bond.h"

using namespace std;

void printFile(string , string );
void cleanFile(string);
void initialBondsFile(string, vector<atom *>, vector<bond *>);
void produceLAMMPSFile(string, vector<atom *>, vector<bond *>);

string outputAtoms(vector<atom*>);
string outputBonds(vector<bond*>);

#endif //C___FILES_REWRITE_OUTPUT_H
