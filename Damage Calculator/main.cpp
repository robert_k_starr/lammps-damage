#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <fstream>
#include <sstream>
#include <dirent.h>
#include <thread>
#include <unistd.h>
#include <future>

#include "functions.hpp"
#include "main.hpp"
#include "filename.hpp"

using namespace std;

int main(int argc, char *argv[]){
	if (argc < 2) cores = 1;
	else cores = stoi(argv[1]);
	if (argc < 3) bincount = 100;
	else bincount = stoi(argv[2]);
	
	unsigned concurentThreadsSupported = std::thread::hardware_concurrency();
	cores = (int) concurentThreadsSupported;

	cout << "Running with " << cores << " cores.\n";
	
	string initfile = "../WalkerFiles/initbonds.txt";
	string outfile = "../omegas.txt";
	ifstream infile(initfile);
	ofstream outputfile;
	outputfile.open(outfile);

	infile >> atomcount >> bondcount >> binmin >> binmax;
	currentbondcount = bondcount;

	float bins[bincount];

	binsize = (binmax - binmin)/(bincount);

	int a, b, c, d;

	// Establish all of the bonds before any damage occurred
	int bonds[bondcount + 1][2];

	while (infile >> a >> b){
		infile >> bonds[a][0];
		infile >> bonds[a][1];
	}

	bondvector = ArraytoVec(bonds, bondcount);

	string base = "../Frames/walkerfile";

	filename walkerlist(base, "id");
	filename bondlist(base, "bonds");
	std::vector<shared_future<vector<float>>> threadList;
	std::vector<vector<float>> results;
	
	while (1){

		// Set new file

		for (int i = 0; i < bincount; i++) bins[i] = 0;
		int j = 0;
		bool stop = 0;
		while(not stop){
			int coresused = 0;
			for (int i = 0; i < cores; i++){
				threadList.push_back(std::async(analyzefile, walkerlist.returnfile(), bondlist.returnfile(), bondvector));
				coresused++;
				walkerlist.nextfolder();
				if (!(bondlist.nextfolder())) {
					stop = 1;
					break;
				}
			}
			
			vector<float> binvec(bincount,0);
			
			for (int i = 0; i < coresused; i++){
			
				vector<float> temp = threadList[i].get();
				
				for (int j = 0; j < bincount; j++) binvec[j] = binvec[j] + temp[j];
			}		
			
			threadList.clear();
			for (int i = 0; i < bincount; i++) bins[i] = bins[i] + binvec[i];
		}

		
		for (int i = 0; i < bincount; i++) {
			outputfile << bins[i]/((float) (walkerlist.folders())) << " ";
		}
		outputfile << "\n";
		outputfile.flush();
		bondlist.nextfile();
		if (!(walkerlist.nextfile())) break;

	}


}
