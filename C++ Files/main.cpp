#include <iostream>
#include <vector>
#include <stdlib.h>

#include "parameters.h"
#include "atom.h"
#include "bond.h"
#include "functions.h"
#include "output.h"

using namespace std;

vector<atom *> atomList;
vector<bond *> bondList;

string damageFolder = "/Users/Robert/Google Drive/Marder/Damage/";
string damageFolderForSystem = "/Users/Robert/Google\\ Drive/Marder/Damage/";

int main() {
    int atomCount = 1;

    for (int i = 0; i < XATOMS; i++){
        for (int j = 0; j < YATOMS; j++){
            for (int k = 0; k < ZATOMS; k++){
                atomList.push_back (new atom(i, j, k));
                atomList.back()->setID(atomCount);
                atomCount++;
            }
        }
    }

    bondList = establishBonds(atomList);

    string systemCommand = "exec rm -r " + damageFolderForSystem + "WalkerFiles/*";

    char systemCommandAsArray[systemCommand.length()];
    strcpy(systemCommandAsArray, systemCommand.c_str());
    system(systemCommandAsArray);

    initialBondsFile(damageFolder + "WalkerFiles/initbonds.txt", atomList, bondList);
	produceLAMMPSFile(damageFolder + "WalkerFiles/noBrokenBonds.txt", atomList, bondList);

    for (int i = 0; i < WALKERFILES; i++) {
        string fileNumber = addZeros(i, WALKERFILES);
        cout << "Producing file " + to_string(i + 1) + "/" + to_string(WALKERFILES) + '\n';
        cout << damageFolder + "WalkerFiles/walkerfile" + fileNumber + ".txt\n";
        performParabolicDeletion(bondList);
        cout << to_string(intactBonds(bondList)) << " out of " << to_string(totalBonds(bondList)) << " bonds remaining.\n";
        produceLAMMPSFile(damageFolder + "WalkerFiles/walkerfile" + fileNumber + ".txt", atomList, bondList);
    }

    return 0;
}