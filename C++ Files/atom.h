//
// Created by Robert Starr on 2019-07-20.
//

#ifndef C___FILES_REWRITE_ATOM_H
#define C___FILES_REWRITE_ATOM_H

#include <vector>
#include <string>

using namespace std;

class atom {
    public:
        atom();
        atom(int, int, int);
        atom(double *);
        std::vector<double> returnAbsoluteCoords();
        double distanceToAtom(atom *);
        void setID(int);
        int returnID();
        string outputAtom();

    private:
        double relativeCoords[3]{};
        double absoluteCoords[3]{};

        int atomID;

        void init(double x, double y, double z);
        void convertToAbsolute(double *);
        void convertToRelative(double *);

        bool realAtom;

};


#endif //C___FILES_REWRITE_ATOM_H
