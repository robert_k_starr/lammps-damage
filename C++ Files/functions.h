//
// Created by Robert Starr on 2019-07-20.
//

#ifndef C___FILES_REWRITE_FUNCTIONS_H
#define C___FILES_REWRITE_FUNCTIONS_H

#include <vector>

#include "atom.h"
#include "bond.h"

using namespace std;

double distance(vector<double>, vector<double>);

vector<bond *> establishBonds(vector<atom *>);

void orderByCoordinate(vector<atom *> &, int);
void orderByCoordinate(vector<atom *> &, int, int, int);
bool checkOrdering(vector<atom *>, int);

void orderByCoordinate(vector<bond *> &, int);
void orderByCoordinate(vector<bond *> &, int, int, int);
bool checkOrdering(vector<bond *>, int);

vector<double> midpoint(atom, atom);
vector<double> midpoint(vector<double>, vector<double>);

void undeleteAllBonds(vector<bond *> &);
double numberToDelete(int, double);
double numberToDeleteParabola(int, double);
void performGaussianDeletion(vector<bond *> &bondList);
void performParabolicDeletion(vector<bond *> &bondList);

double zScore(double, double);
void deleteXBondsInColumn(vector<bond *>, int);

int totalBonds(vector<bond *>);
int intactBonds(vector<bond *>);
void reIDBonds(vector<bond *>&);

string addZeros(int, int);

#endif //C___FILES_REWRITE_FUNCTIONS_H
