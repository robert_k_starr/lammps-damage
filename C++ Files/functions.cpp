//
// Created by Robert Starr on 2019-07-20.
//

#include <math.h>
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <cstring>

#include "parameters.h"
#include "functions.h"

double distance(vector<double> coords1, vector<double> coords2){
    double sumOfSquares = 0;
    double test = coords1[1];
    for (int i = 0; i < 3; i++) {
        double difference = coords1[i] - coords2[i];
        sumOfSquares += pow(difference, 2);
    }

    return sqrt(sumOfSquares);
}

vector<bond *> establishBonds(vector<atom *> atomList) {
    vector<bond *> bondList;
    int numberOfAtoms = atomList.size();
    int bondCount = 0;

    orderByCoordinate(atomList, 0);

    for (int i = 0; i < numberOfAtoms; i++){
        for (int j = i + 1; j < numberOfAtoms; j++) {
            if (atomList[i]->returnAbsoluteCoords()[0] + SPACING + BUFFER <
                atomList[j]->returnAbsoluteCoords()[0]) {
                break;
            }
            if (atomList[i]->distanceToAtom(atomList[j]) <= SPACING + BUFFER) {
                bondList.push_back(new bond(atomList[i], atomList[j]));
                bondList[bondCount]->setID(bondCount);
                bondList[bondCount]->setType(1);
                bondCount++;
            }
        }
    }

    return bondList;
}

void orderByCoordinate(vector<atom *> &atomList, int coordinate){
    srand (time(NULL));

    orderByCoordinate(atomList, coordinate, 0, atomList.size() - 1);

    if (!checkOrdering(atomList, coordinate)){
        cout << "ERROR: Atom ordering algorithm didn't work." << "/n";
        exit(EXIT_FAILURE);
    }

}

void orderByCoordinate(vector<atom *> &atomList, int coordinate, int start, int end){
    if ((start < end) && (start < atomList.size())){
        int pivot = rand() % (end + 1 - start) + start;
        swap(atomList[start], atomList[pivot]);
        double coordinateValue = atomList[start]->returnAbsoluteCoords()[coordinate];
        int j = end;
        for (int i = start + 1; i < j;) {
            bool lessThanOrEqual = atomList[i]->returnAbsoluteCoords()[coordinate] <=
                                   coordinateValue;
            if (lessThanOrEqual) i++;
            else {
                swap(atomList[i], atomList[j]);
                j--;
            }
        }

        if (coordinateValue < atomList[j]->returnAbsoluteCoords()[coordinate]) {
            j--;
        }

        swap(atomList[start], atomList[j]);

        orderByCoordinate(atomList, coordinate, start, j -1);
        orderByCoordinate(atomList, coordinate, j + 1, end);
    }
}

void orderByCoordinate(vector<bond *> & bondList, int coordinate) {

    srand (time(NULL));

    orderByCoordinate(bondList, coordinate, 0, bondList.size() - 1);

    if (!checkOrdering(bondList, coordinate)){
        cout << "ERROR: Bond ordering algorithm didn't work." << "/n";
        exit(EXIT_FAILURE);
    }
}

void orderByCoordinate(vector<bond *> &bondList, int coordinate, int start, int end) {
    if ((start < end) && (start < bondList.size())){
        int pivot = rand() % (end + 1 - start) + start;
        swap(bondList[start], bondList[pivot]);
        double coordinateValue = bondList[start]->bondMidpoint()[coordinate];
        int j = end;
        for (int i = start + 1; i < j;) {
            bool lessThanOrEqual = bondList[i]->bondMidpoint()[coordinate] <=
                                   coordinateValue;
            if (lessThanOrEqual) i++;
            else {
                swap(bondList[i], bondList[j]);
                j--;
            }
        }

        if (coordinateValue < bondList[j]->bondMidpoint()[coordinate]) {
            j--;
        }

        swap(bondList[start], bondList[j]);

        orderByCoordinate(bondList, coordinate, start, j -1);
        orderByCoordinate(bondList, coordinate, j + 1, end);
    }
}

bool checkOrdering(vector<atom *> atomList, int coordinate){
    int vectorLength = atomList.size();
    for (int i = 1; i < vectorLength; i++){
        if (atomList[i]->returnAbsoluteCoords()[coordinate] <
            atomList[i - 1]->returnAbsoluteCoords()[coordinate]){
            return false;
        }
    }
    return true;
}

bool checkOrdering(vector<bond *> bondList, int coordinate) {
    int vectorLength = bondList.size();
    for (int i = 1; i < vectorLength; i++){
        if (bondList[i]->bondMidpoint()[coordinate] <
            bondList[i - 1]->bondMidpoint()[coordinate]){
            return false;
        }
    }
    return true;
}

vector<double> midpoint(atom atom1, atom atom2) {
    vector<double> atom1Coords = atom1.returnAbsoluteCoords();
    vector<double> atom2Coords = atom2.returnAbsoluteCoords();

    return midpoint(atom1Coords, atom2Coords);
}

vector<double> midpoint(vector<double> coords1, vector<double> coords2) {
    vector<double> finalMidpoint;

    if (coords1.size() != coords2.size()){
        cout << "ERROR: Can't find midpoint of two vectors of different sizes." << "/n";
        exit(EXIT_FAILURE);
    }
    int length = coords1.size();

    for (int i = 0; i < length; i++) {
        double tempCoordinate = (coords1[i] + coords2[i])/2.0;

        finalMidpoint.push_back(tempCoordinate);
    }

    return finalMidpoint;
}

void undeleteAllBonds(vector<bond *> &bondList){
    int length = bondList.size();

    for (int i = 0; i < length; i++) bondList[i]->unDeleteBond();
}

void performGaussianDeletion(vector<bond *> &bondList) {
    double centerPoint = XATOMS*SPACING/2.0;

    undeleteAllBonds(bondList);
    orderByCoordinate(bondList, 0);

    int length = bondList.size();
    vector<bond *> bondColumn;

    for (int i = 0; i < length; i++){
        if ((bondColumn.size() != 0) && (bondColumn.back()->bondMidpoint()[0] != bondList[i]->bondMidpoint()[0])) {
            deleteXBondsInColumn(bondColumn,
                    numberToDelete(bondColumn.size(), zScore(centerPoint, bondColumn.back()->bondMidpoint()[0])));

            bondColumn.clear();
        }
        bondColumn.push_back(bondList[i]);
    }

    deleteXBondsInColumn(bondColumn,
                         numberToDelete(bondColumn.size(), zScore(centerPoint, bondColumn.back()->bondMidpoint()[0])));
}

void performParabolicDeletion(vector<bond *> &bondList) {
    double centerPoint = XATOMS*SPACING/2.0;

    undeleteAllBonds(bondList);
    orderByCoordinate(bondList, 0);

    int length = bondList.size();
    vector<bond *> bondColumn;

    for (int i = 0; i < length; i++){
        if ((bondColumn.size() != 0) && (bondColumn.back()->bondMidpoint()[0] != bondList[i]->bondMidpoint()[0])) {
            deleteXBondsInColumn(bondColumn,
                    numberToDeleteParabola(bondColumn.size(), zScore(centerPoint, bondColumn.back()->bondMidpoint()[0])));

            bondColumn.clear();
        }
        bondColumn.push_back(bondList[i]);
    }

    deleteXBondsInColumn(bondColumn,
                         numberToDeleteParabola(bondColumn.size(), zScore(centerPoint, bondColumn.back()->bondMidpoint()[0])));
}

double zScore(double centerPoint, double currentPosition) {
    return (centerPoint - currentPosition)/SIGMA;
}

void deleteXBondsInColumn(vector<bond *> bondColumn, int numberToDelete) {
    srand(time(NULL));
    int length = bondColumn.size();
    int bondsLeftToDelete = numberToDelete;

    while (bondsLeftToDelete > 0) {
        int randomBond = rand() % length;

        if (bondColumn[randomBond]->isActive()) {
            bondColumn[randomBond]->deleteBond();
            bondsLeftToDelete--;
        }
    }
}

double numberToDelete(int length, double zScore) {
    double percentage = GAUSSHEIGHT*exp(-1*pow(zScore, 2)/2.0);

    return (int) round(percentage*length);
}

double numberToDeleteParabola(int length, double zScore){
	if (zScore > 1) return 0;
	double percentage = GAUSSHEIGHT*(1 - pow(zScore,2));
	
	return (int) round(percentage*length);
}

int totalBonds(vector<bond *> bondList) {
    return intactBonds(bondList) + bondList.size();
}

int intactBonds(vector<bond *> bondList) {
    int length = bondList.size();
    int currentCount = 0;

    for (int i = 0; i < length; i++) {
        if (bondList[i]->isActive()) currentCount++;
    }

    return currentCount;
}

void reIDBonds(vector<bond *> &bondList) {
    int currentID = 1;
    int length = bondList.size();
    for (int i = 0; i < length; i++) {
        if (bondList[i]->isActive()){
            bondList[i]->setID(currentID++);
        } else {
            bondList[i]->setID(-1);
        }
    }
}

string addZeros(int numberToConvert, int totalInList) {
    string finalNumber = "";

    int numberOfZeros = to_string(totalInList - 1).length() - to_string(numberToConvert).length();

    for (int i = 0; i < numberOfZeros; i++) finalNumber = finalNumber + "0";
    finalNumber = finalNumber + to_string(numberToConvert);

    return finalNumber;
}
