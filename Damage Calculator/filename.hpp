#ifndef filename_hpp
#define filename_hpp

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>

using namespace std;

class filename {
		string base, type;
		int folder, digits, currentfile, maxfile, maxfolder;
		vector <string> files;

	public:
		filename(string, string);
		string returnfolder();
		string returnfile();

		bool nextfolder();
		bool nextfile();
		int folders();

};

#endif /* filename_hpp */
