import sys, os, random
from math import *
from PIL import Image, ImageDraw
from walkervariables import *

random.seed()

def cleanfiles(dir):
	deletebox = []

	for files in os.listdir('./' + dir):
		if files[0] != ('.'):
			deletebox.append(files)

	for files in deletebox:
		os.remove('./' + dir + '/' + files)

cleanfiles('Walkers')
cleanfiles('WalkersDebug')

print "Files cleaned"

lattice = []
list = []
holes = []

RATIO = min(float(2*SCREENWIDTH)/(COLUMNS + 2), float(SCREENHEIGHT)/((ROWS + 2)*sqrt(3)))

SCREENWIDTH = int((COLUMNS + 2)*RATIO/2)
SCREENHEIGHT = int((ROWS + 2)*sqrt(3)*RATIO)

SCREENSIZE = (SCREENWIDTH, SCREENHEIGHT)

CURRENTFRAME = 0

class particle:
	def __init__(self, pos1, pos2):
		self.x = (float(pos1)/2)
		self.y = (float(float(pos2) + float(pos1%2)/2)*sqrt(3))
		self.color = BLUE
		self.neighbors = []
		self.highodds = []
		self.oldneighbors = []
		self.end = False
		self.id = 0
	
	def addneighbor(self, atom):
		self.neighbors.append(atom)
		atom.neighbors.append(self)
	
	def delete(self):
		self.color = RED
		for atoms in self.neighbors:
			atoms.neighbors.remove(self)
			atoms.oldneighbors.append(self)
		for atoms in self.oldneighbors:
			atoms.neighbors.remove(self)
			atoms.oldneighbors.append(self)			
		for columns in lattice:
			if self in columns:
				columns.remove(self)
				if len(columns) == 0:
					lattice.remove(columns)
		added = False
		for columns in holes:
			for atoms in columns:
				if atoms in self.oldneighbors:
					columns.append(self)
					added = True
		if added == False:
			newarray = [self]
			holes.append(newarray)
	
	def breakbond(self, atom):
		self.neighbors.remove(atom)
		atom.neighbors.remove(self)

def setids(list):
	atomid = 0
	for atom in list:
		atomid = atomid + 1
		atom.id = atomid
	return atomid

def checkodds(atom1, atom2):
	if atom1.y == atom2.y:
		return
	if atom1.y > atom2.y:
		check = 1
	else:
		check = -1
	for atoms in atom1.neighbors:
		if check*atoms.y < check*atom1.y:
			atoms.highodds.append(atom1)
			atom1.highodds.append(atoms)
	for atoms in atom2.neighbors:
		if check*atoms.y > check*atom2.y:
			atoms.highodds.append(atom2)
			atom2.highodds.append(atoms)			
							
def screenshot(frame):
	screen = Image.new('RGB', SCREENSIZE, BLACK)
	for atom1 in list:
		for atom2 in atom1.neighbors:
			if atom2 in atom1.highodds:
				linedraw(atom1, atom2, screen, color = RED)
			else:
				linedraw(atom1, atom2, screen)
	for row in lattice:
		for atom in row:
			plot(atom, ATOMRADIUS, screen)
	for row in holes:
		for atom in row:
			plot(atom, ATOMRADIUS, screen)
	screen.save("./Walkers/test" + str(frame).zfill(5) + ".jpg", "JPEG")
	del screen

def inbounds(atom, x, y):
	if atom.x < x[0] or atom.x > x[1]:
		return False
	elif atom.y < y[0] or atom.y > y[1]:
		return False
	else:
		return True

def debug(origin, frame, zoom = 10):
	screen = Image.new('RGB', SCREENSIZE, BLACK)
	
	xbound = (origin[0] - zoom, origin[0] + zoom)
	ybound = (origin[1] - zoom, origin[1] + zoom)
	start = (xbound[0], ybound[0])
	
	ratio = int(min(SCREENSIZE[0], SCREENSIZE[1])/(2*zoom))
	
	for row in lattice:
		for atom in row:
			if inbounds(atom, xbound, ybound):
				plot(atom, ATOMRADIUS, screen, ratio, start)
	
	for row in holes:
		for atom in row:
			if inbounds(atom, xbound, ybound):
				plot(atom, ATOMRADIUS, screen, ratio, start)
	
	screen.save("./WalkersDebug/test" + str(frame).zfill(5) + ".jpg", "JPEG")
	del screen
			
def distance(particle1, particle2):
	xdis = particle1.x - particle2.x
	ydis = particle1.y - particle2.y

	if (particle1.y == 0):
		ydis = min(abs(ROWS*sqrt(3) - particle2.y), abs(ydis))
		
	if (particle2.y == 0):
		ydis = min(abs(ROWS*sqrt(3) - particle1.y), abs(ydis))

	totdis = hypot(xdis, ydis)
	return totdis

def plot(atom, radius, drawing, ratio = RATIO, offset = (0, 0)):
	xstart = (1 + atom.x - offset[0])*ratio - radius
	ystart = (1 + atom.y - offset[1])*ratio - radius
	xend = (1 + atom.x - offset[0])*ratio + radius
	yend = (1 + atom.y - offset[1])*ratio + radius
	coords = (xstart, ystart, xend, yend)
	draw = ImageDraw.Draw(drawing)
	draw.ellipse([(xstart, ystart), (xend, yend)], fill=atom.color)

def linedraw(atom1, atom2, drawing, ratio = RATIO, offset = (0, 0), color = WHITE):
	xstart = (1 + atom1.x - offset[0])*ratio
	ystart = (1 + atom1.y - offset[1])*ratio
	xend = (1 + atom2.x - offset[0])*ratio
	yend = (1 + atom2.y - offset[1])*ratio
	draw = ImageDraw.Draw(drawing)
	draw.line([(xstart, ystart), (xend, yend)], width = 2, fill = color)
						
for x in range(0, COLUMNS):
	a = []
	for y in range(0, ROWS):
		b = particle(x, y)
		a.append(b)
		list.append(b)
	lattice.append(a)
	
mindistance = 1000

print "Calculating neighbors..."

for row1 in lattice:
	row = lattice.index(row1)
	for atom1 in row1:
		atom = row1.index(atom1)
		for number1 in range(row - 1, row + 3):
			if number1 >= len(lattice) or number1 < 0:
				continue
			for number2 in range(atom - 1, atom + 3):
				if number2 >= len(lattice[number1]) or number2 < 0:
					continue
				atom2 = lattice[number1][number2]
				if atom1 != atom2:
					if distance(atom1, atom2) <= 1.1 and atom1 not in atom2.neighbors:
						atom1.addneighbor(atom2)
mindist = 100000
miny = 100000
maxy = 0

for atoms in lattice:
	for otheratoms in lattice:
		atom1 = atoms[0]
		atom2 = otheratoms[-1]
		#print "(" + str(atom1.x) + ", " + str(atom1.y) + ") (" + str(atom2.x) + ", " + str(atom2.y) + ")"
		mindist = min(mindist, distance(atom1, atom2))
		miny = min(miny, atom1.y, atom2.y)
		maxy = max(maxy, atom1.y, atom2.y)
		#print distance(atom1, atom2)
		atom1.color = GREEN
		atom2.color = GREEN
		if distance(atom1, atom2) <= 1.1 and atom1 not in atom2.neighbors:
			atom1.addneighbor(atom2)


screenshot(CURRENTFRAME)
CURRENTFRAME = CURRENTFRAME + 1

for walker in range(0, WALKERS):
	print "Sending out walker # " + str(walker + 1) + "/" + str(WALKERS)
	hungry = True
	startx = int(len(lattice)/2)
	starty = int(len(lattice[startx])/2)
	walkeratom = lattice[startx][starty]
	walkeratom.color = GREEN
	step = 0
	odds = ODDS
	DEBUG = False
	atomorigin = (0, 0)
	while hungry:
		atomneighbor = random.choice(walkeratom.neighbors)
		if atomneighbor in walkeratom.highodds:
			odds = VERYHIGHODDS
		else:
			odds = ODDS
		step = step + 1
		if float(random.randint(0, 99999))/100000 < odds:
			#print "YUM YUM YUM, I ate an atom at (" + str(walkeratom.x) + ", " + str(walkeratom.y) + ")"
			walkeratom.breakbond(atomneighbor)
			walkeratom.color = BLUE
			checkodds(walkeratom, atomneighbor)
			hungry = False
		elif (walkeratom.neighbors != []):
			walkeratom.color = BLUE
			walkeratom = atomneighbor
			walkeratom.color = GREEN
		else:
			walkeratom.delete()
			hungry = False
		if DEBUG:
			debug(atomorigin, CURRENTFRAME)
		if MOVIE:
			screenshot(CURRENTFRAME)
		CURRENTFRAME = CURRENTFRAME + 1

walkerfile = open("./walkerfile.txt", "w")

walkerfile.truncate()

data = "Atoms\n\n"

numatoms = setids(list)

##Establish atoms list
for atoms in list:
	data = data + str(atoms.id) + " 0 1 "
	data = data + str(atoms.x) + " "
	data = data + str(atoms.y) + " 0.0\n"
	
data = data + "\nBonds\n\n"

bonds = 0

##Establish bond list
for atoms in list:
	for neigh in atoms.neighbors:
		if atoms.id < neigh.id:
			bonds = bonds + 1
			bondtype = 1
			data = data + str(bonds) + " " + str(bondtype) + " "
			data = data + str(atoms.id) + " " + str(neigh.id) + "\n"

walkerfile.write("LAMMPS Data File \n\n")
walkerfile.write(str(numatoms) + " atoms\n")
walkerfile.write(str(bonds) + " bonds\n")
walkerfile.write("12 extra bond per atom\n") ## THESE NEED MODIFICATION
walkerfile.write("3.0 atom types\n") ## THESE NEED MODIFICATION
walkerfile.write("3.0 bond types\n\n") ## THESE NEED MODIFICATION

walkerfile.write("0.0 " + str(COLUMNS/2) + " xlo xhi\n")
walkerfile.write("0.0 " + str(ROWS*sqrt(3)) + " ylo yhi\n")
walkerfile.write("0.0 1.0 zlo zhi\n\n")

walkerfile.write(data + "\n")

walkerfile.close()

if MOVIE:
	os.chdir("./Walkers")
	os.system("ffmpeg -framerate 30 -i test%05d.jpg -c:v libx264 -r 30 _movie.mp4")

screenshot(CURRENTFRAME)
