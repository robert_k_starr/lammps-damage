//
// Created by Robert Starr on 2019-07-20.
//

#ifndef C___FILES_REWRITE_BOND_H
#define C___FILES_REWRITE_BOND_H

#include <vector>
#include <string>

#include "atom.h"

using namespace std;

class bond {

    public:
        bond(atom *, atom *);
        int returnID();
        void setID(int);
        void setType(int);

        vector<double> bondMidpoint();
        string outputBond();

        void deleteBond();
        void unDeleteBond();

        bool isActive();

    private:
        atom * atom1;
        atom * atom2;
        int id;
        int type;
        bool active;
};


#endif //C___FILES_REWRITE_BOND_H
